package com.team10.placery.helper;

import android.location.Location;

public class LatlngFormater {

    public static String getDMS(Location location) {
        double lat = location.getLatitude();
        double lng = location.getLongitude();

        String latStr = ddToDms (lat, "N", "S");
        String lngStr = ddToDms(lng, "E", "W");

        return latStr + ", " + lngStr;
    }
    public static String getDd(Location location) {
        return location.getLatitude() + "°, " + location.getLongitude() + "°";
    }

    public static String ddToDms(double coord, String apPos, String apNeg) {
        double dd = Math.abs(coord);
        String secFormat = "%1$,.3f";
        String apHem = (coord > 0) ? apPos : apNeg;

        int deg = (int)dd;
        int min = (int)(60 * (dd - deg));
        double sec = (dd - deg - min/60.) * 3600;
        return deg + "° " + min + "' " + String.format(secFormat, sec) + "\" " + apHem;
    }
}
