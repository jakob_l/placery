package com.team10.placery;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class MyItem implements ClusterItem {
    private final LatLng position;
    private final String title;
    private final String snippet;
    private final int id;

    public MyItem(LatLng position, String title, String snippet, int id) {
        this.position = position;
        this.title = title;
        this.snippet = snippet;
        this.id = id;
    }

    @Override
    public LatLng getPosition() {

        return position;
    }

    @Override
    public String getTitle() {

        return title;
    }

    @Override
    public String getSnippet() {

        return snippet;
    }

    public String getId() {

        return String.valueOf(id) ;
    }
}