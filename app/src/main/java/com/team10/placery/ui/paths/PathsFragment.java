package com.team10.placery.ui.paths;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.team10.placery.R;
import com.team10.placery.model.AppDatabase;
import com.team10.placery.model.Path;
import com.team10.placery.model.PathDAO;
import com.team10.placery.model.PathWithEntries;

import java.util.List;

public class PathsFragment extends Fragment {
    private static final String TAG = "Fragment Paths";

    public View onCreateView(
            @NonNull LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState
    ) {

        View root = inflater.inflate(R.layout.fragment_paths, container, false);

        RecyclerView recyclerView = root.findViewById(R.id.paths_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(root.getContext()));

        PathDAO dao =  AppDatabase.getInstance(root.getContext()).getPathDAO();
        List<PathWithEntries> pathList = dao.getAllPathEntries();
        PathsListAdapter pathsListAdapter = new PathsListAdapter(getContext(), pathList);
        recyclerView.setAdapter(pathsListAdapter);

        return root;
    }
}