package com.team10.placery.ui.home;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.snackbar.Snackbar;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.team10.placery.MyItem;
import com.team10.placery.R;
import com.team10.placery.ui.entry.SingleEntryActivity;
import com.team10.placery.helper.LatlngFormater;
import com.team10.placery.model.AppDatabase;
import com.team10.placery.model.EntryDAO;
import com.team10.placery.model.EntryWithPaths;
import com.team10.placery.model.PathDAO;
import com.team10.placery.model.PathWithEntries;

import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.team10.placery.model.EntryWithPaths.byCreatedDateTime;

public class HomeFragment extends Fragment implements OnMapReadyCallback {
    private static final String TAG = "HomeFragment";

    private FusedLocationProviderClient fusedLocationProviderClient;
    private GoogleMap gMap;

    private final LatLng defaultLocation = new LatLng(-33.8523341, 151.2106085);
    private static final int DEFAULT_ZOOM = 15;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private boolean locationPermissionGranted;


    private Location lastKnownLocation;

    private List<PathWithEntries> pathList;
    private List<EntryWithPaths> entryList;

    private BottomSheetBehavior<View> bottomSheetBehavior;
    public class CustomClusterRenderer extends DefaultClusterRenderer<MyItem> {

        private final Context mContext;
        private final IconGenerator mClusterIconGenerator;

        public CustomClusterRenderer(Context context, GoogleMap map,
             ClusterManager<MyItem> clusterManager) {

            super(context, map, clusterManager);

            mContext = context;
            mClusterIconGenerator = new IconGenerator(mContext.getApplicationContext());
        }

        @Override
        protected void onBeforeClusterItemRendered(@Nullable MyItem item,
             MarkerOptions markerOptions) {

            markerOptions.icon(bitmapDescriptorFromVector(getContext()));
        }

        @Override
        protected void onClusterItemRendered(@NotNull MyItem item, @NotNull Marker marker){

            super.onClusterItemRendered(item, marker);
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<MyItem> cluster,
             MarkerOptions markerOptions) {

            mClusterIconGenerator.setBackground(
                    ContextCompat.getDrawable(mContext, R.drawable.cluster_circle));

            mClusterIconGenerator.setTextAppearance(R.style.WhiteTextAppearance);

            mClusterIconGenerator.setContentPadding(50, 30, 50, 30);
            final Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context){
        Drawable vectorDrawable = ContextCompat.getDrawable(context, R.drawable.ic_entry_mapmarker);
        vectorDrawable.setBounds(0,0,vectorDrawable.getIntrinsicWidth(),vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),vectorDrawable.getIntrinsicHeight(),Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        // Construct a FusedLocationProviderClient.
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());

        // Create the Map.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.home_map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        View bottomSheet = root.findViewById(R.id.bottom_sheet);
        TextView dateTime = bottomSheet.findViewById(R.id.bs_date_time);
        TextView latLngDd = bottomSheet.findViewById(R.id.bs_latlngDd);
        TextView latLngDms = bottomSheet.findViewById(R.id.bs_latlngDms);
        View btnBottomSheet = root.findViewById(R.id.fab_dna);
        View btnCancel = root.findViewById(R.id.btn_cancel);
        View btnAdd = root.findViewById(R.id.btn_add_DNA);
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);

        btnBottomSheet.setOnClickListener(v -> {
            getLocationPermission();
            getDeviceLocation();
            updateLocationUI();
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("E, dd MMM uuuu, HH:mm");
            LocalDateTime now = LocalDateTime.now();
            dateTime.setText(dtf.format(now));
            latLngDms.setText(lastKnownLocation == null ? "--" : LatlngFormater.getDMS(lastKnownLocation));
            latLngDd.setText(lastKnownLocation == null ? "--" : LatlngFormater.getDd(lastKnownLocation));

            bottomSheetBehavior.setPeekHeight(400);
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        });

        btnAdd.setOnClickListener(v -> {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            Snackbar.make(getActivity().findViewById(R.id.main_container),
                R.string.DNA_added,
                Snackbar.LENGTH_SHORT
            ).show();
        });

        btnCancel.setOnClickListener(v -> bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN));

        EntryDAO entryDao =  AppDatabase.getInstance(root.getContext()).getEntryDAO();
        entryList = entryDao.getAllEntries();
        PathDAO pathDao =  AppDatabase.getInstance(root.getContext()).getPathDAO();
        pathList = pathDao.getAllPathEntries();

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onViewCreated - view: " + view);
        super.onViewCreated(view, savedInstanceState);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void onMapReady(GoogleMap gMap) {
        this.gMap = gMap;
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            gMap.setMyLocationEnabled(true);
            gMap.getUiSettings().setMyLocationButtonEnabled(true);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1340);
        }
        try {
            boolean success = gMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getContext(), R.raw.style_json));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ");
        }

        getLocationPermission();
        updateLocationUI();
        getDeviceLocation();

        gMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        LatLng latLngNow = new LatLng(entryList.get(0).entry.lat, entryList.get(0).entry.lon);
        Log.d(TAG, "latlong" + latLngNow);
        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngNow, 13));
        gMap.setPadding(0,getResources().getDimensionPixelSize(R.dimen.actionBarOffset),0,0);

        ClusterManager<MyItem> clusterManager;
        clusterManager = new ClusterManager<>(getContext(), gMap);

        final CustomClusterRenderer renderer = new CustomClusterRenderer(getContext(), gMap, clusterManager);

        clusterManager.setRenderer(renderer);

        // Loop over all entries to draw MapMarker on Map
        for(EntryWithPaths entryEl: entryList){
            LatLng latLngEntry = new LatLng(entryEl.entry.lat, entryEl.entry.lon);
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd MMM uuuu");
            clusterManager.addItem(new MyItem(latLngEntry, entryEl.entry.title, entryEl.getCreatedLocalDateTime().format(dtf), entryEl.entry.ID));
        }

        clusterManager.setOnClusterItemInfoWindowClickListener(
                new ClusterManager.OnClusterItemInfoWindowClickListener<MyItem>() {
                    @Override public void onClusterItemInfoWindowClick(MyItem stringClusterItem) {

                        Intent intent = new Intent(getContext(), SingleEntryActivity.class);
                        intent.putExtra("entryId", stringClusterItem.getId());
                        getContext().startActivity(intent);
                    }
                });

        gMap.setOnCameraIdleListener(clusterManager);

        // Loop over all Paths to draw Polyline on Map
        for(PathWithEntries pathEl: pathList){
            List<LatLng> pathLineList = new ArrayList<>();
            List<EntryWithPaths> entriesOfPath = pathEl.entriesWithPaths;
            Collections.sort(entriesOfPath, byCreatedDateTime.reversed() );
            for(EntryWithPaths entryEl: entriesOfPath){
                LatLng latLngEntry = new LatLng(entryEl.entry.lat, entryEl.entry.lon);
                pathLineList.add(latLngEntry);
            }
            gMap.addPolyline(new PolylineOptions()
                    .clickable(true)
                    .color(R.color.primaryColor)
                    .width(4)
                    .addAll(pathLineList)
            );
        }
    }

    private void getDeviceLocation() {
        try {
            if (locationPermissionGranted) {
                Task<Location> locationResult = fusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(getActivity(), task -> {
                    if (task.isSuccessful()) {
                        // Set the map's camera position to the current location of the device.
                        lastKnownLocation = task.getResult();
                        if (lastKnownLocation != null) {
                            gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                                    new LatLng(lastKnownLocation.getLatitude(),
                                            lastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                        }
                    } else {
                        Log.d(TAG, "Current location is null. Using defaults.");
                        Log.e(TAG, "Exception: %s", task.getException());
                        gMap.animateCamera(CameraUpdateFactory
                                .newLatLngZoom(defaultLocation, DEFAULT_ZOOM));
                        gMap.getUiSettings().setMyLocationButtonEnabled(false);
                    }
                });
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage(), e);
        }
    }

    private void getLocationPermission() {
        if (ContextCompat.checkSelfPermission(getContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            locationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        locationPermissionGranted = false;
        if (requestCode == PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    locationPermissionGranted = true;
                }
        }
        updateLocationUI();
    }

    private void updateLocationUI() {
        if (gMap == null) {
            return;
        }
        try {
            if (locationPermissionGranted) {
                gMap.setMyLocationEnabled(true);
                gMap.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                gMap.setMyLocationEnabled(false);
                gMap.getUiSettings().setMyLocationButtonEnabled(false);
                lastKnownLocation = null;
                getLocationPermission();
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }
}