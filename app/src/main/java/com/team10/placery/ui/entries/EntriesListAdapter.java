package com.team10.placery.ui.entries;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.squareup.picasso.Picasso;
import com.team10.placery.R;
import com.team10.placery.ui.entry.SingleEntryActivity;
import com.team10.placery.model.EntryWithPaths;

import org.jetbrains.annotations.NotNull;

import java.time.format.DateTimeFormatter;
import java.util.List;

public class EntriesListAdapter extends RecyclerView.Adapter<EntriesListAdapter.MyViewHolder> {
    private static final String TAG = "EntriesListAdapter";

    private final List<EntryWithPaths> entriesData;
    private final Context context;

    public EntriesListAdapter(Context context, List<EntryWithPaths> entries) {
        this.entriesData = entries;
        this.context = context;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView date;
        public ImageView thumb;
        public RelativeLayout coordinates;
        public TextView lat;
        public TextView lon;
        public ImageView icon;
        public CardView entryCard;

        public MyViewHolder(MaterialCardView v) {
            super(v);
            title = v.findViewById(R.id.entry_title);
            date = v.findViewById(R.id.entry_date);
            thumb = v.findViewById(R.id.entry_image);
            icon = v.findViewById(R.id.entry_type_icon);
            coordinates = v.findViewById(R.id.entry_coordinates);
            lat = v.findViewById(R.id.lat);
            lon = v.findViewById(R.id.lon);
            entryCard = itemView.findViewById(R.id.entry_item_card);
        }
    }

    @NotNull
    @Override
    public EntriesListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MaterialCardView view = (MaterialCardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.entry_list_item, parent, false);
        return new MyViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.title.setText(entriesData.get(position).entry.title);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("E, dd MMM uuuu, HH:mm");
        holder.date.setText(entriesData.get(position).getCreatedLocalDateTime().format(dtf));
        if (!entriesData.get(position).paths.isEmpty()) {
            holder.icon.setImageResource(R.drawable.ic_entry_of_path);
        } else {
            holder.icon.setImageResource(R.drawable.ic_baseline_bookmark_border);
        }
        if (entriesData.get(position).entry.mainImage != null) {
            Picasso
                .get()
                .load(entriesData.get(position).entry.mainImage)
                .fit()
                .centerCrop()
                .into(holder.thumb);
            holder.thumb.setVisibility(View.VISIBLE);
        } else {
            holder.coordinates.setVisibility(View.VISIBLE);
            holder.lat.setText(entriesData.get(position).entry.lat.toString());
            holder.lon.setText(entriesData.get(position).entry.lon.toString());
        }

        holder.entryCard.setOnClickListener(view -> {
            String id = entriesData.get(position).entry.ID.toString();
            Log.d(TAG, "onClick: clicked on: Entry with ID " + id);
            Intent intent = new Intent(context, SingleEntryActivity.class);
            intent.putExtra("entryId", id);
            context.startActivity(intent);
        });

    }

    @Override
    public int getItemCount() {
        return entriesData.size();
    }
}