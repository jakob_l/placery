package com.team10.placery.ui.entry;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.team10.placery.R;
import com.team10.placery.helper.LatlngFormater;
import com.team10.placery.model.AppDatabase;
import com.team10.placery.model.EntryDAO;
import com.team10.placery.model.EntryWithPaths;
import com.team10.placery.model.Media;

import org.jetbrains.annotations.NotNull;

import java.time.format.DateTimeFormatter;
import java.util.List;

public class SingleEntryActivity extends AppCompatActivity {
    private static final String TAG = "SingleEntryActivity";

    private Float lat = null;
    private Float lon = null;

    private final OnMapReadyCallback callback = gMap -> {
        try {
            // Customise the styling of the base map
            boolean success = gMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getApplicationContext(), R.raw.style_json));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ");
        }

        gMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        gMap.getUiSettings().setMyLocationButtonEnabled(true);

        LatLng entryPoint = new LatLng(lat, lon);
        gMap.addMarker(new MarkerOptions().icon(bitmapDescriptorFromVector(getApplicationContext())).position(entryPoint));
        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(entryPoint, 15));
    };

    private BitmapDescriptor bitmapDescriptorFromVector(Context context){
        Drawable vectorDrawable = ContextCompat.getDrawable(context, R.drawable.ic_entry_mapmarker);
        vectorDrawable.setBounds(0,0,vectorDrawable.getIntrinsicWidth(),vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),vectorDrawable.getIntrinsicHeight(),Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry_single_view);

        Intent intent = getIntent();
        String id = intent.getStringExtra("entryId");
        Log.d(TAG, "ID: " + id);

        EntryDAO dao =  AppDatabase.getInstance(getApplicationContext()).getEntryDAO();
        EntryWithPaths entryWithPaths = dao.getEntryByID(id);
        List<Media> mediaList = entryWithPaths.media;
        Log.d(TAG, "entry: " + entryWithPaths);

        TextView entryTitle = findViewById(R.id.single_entry_title);
        TextView entrySubtitle = findViewById(R.id.single_entry_subtitle);
        TextView entryDate = findViewById(R.id.single_entry_date);
        TextView entrylatLngDd = findViewById(R.id.single_entry_latlngDd);
        TextView entryLatLngDms = findViewById(R.id.single_entry_latlngDms);
        TextView entryContent = findViewById(R.id.single_entry_textContent);
        LinearLayout entryPlaceholder = findViewById(R.id.single_entry_content_placeholder);
        ImageView titleImage = findViewById(R.id.single_entry_hero);
        TextView mediaTitle = findViewById(R.id.single_entry_h_media);

        entryTitle.setText(entryWithPaths.entry.title);
        entrySubtitle.setText(entryWithPaths.entry.locationName);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("E, dd MMM uuuu, HH:mm");
        entryDate.setText(entryWithPaths.entry.createDate.format(dtf));
        lat = entryWithPaths.entry.lat;
        lon = entryWithPaths.entry.lon;
        Location entryLocation = new Location("");
        entryLocation.setLatitude(lat);
        entryLocation.setLongitude(lon);
        entrylatLngDd.setText(LatlngFormater.getDd(entryLocation));
        entryLatLngDms.setText(LatlngFormater.getDMS(entryLocation));
        String text = entryWithPaths.entry.textContent;
        if (text != null) {
            entryPlaceholder.setVisibility(View.GONE);
            entryContent.setText(Html.fromHtml(text));
        } else {
            entryContent.setVisibility(View.GONE);
        }
        if (entryWithPaths.entry.mainImage != null) {
            Picasso
                .get()
                .load(entryWithPaths.entry.mainImage)
                .fit()
                .centerCrop()
                .into(titleImage);
            titleImage.setVisibility(View.VISIBLE);
            titleImage.setContentDescription("Image for Entry  \"" + entryWithPaths.entry.title +  "\"");
        } else {
            LinearLayout.LayoutParams parameter =  (LinearLayout.LayoutParams) entryTitle.getLayoutParams();
            Log.d(TAG, parameter.leftMargin + " " + getResources().getDimensionPixelSize(R.dimen.spacer_l)  );
            parameter.setMargins(  // left, top, right, bottom
                    parameter.leftMargin,
                    getResources().getDimensionPixelSize(R.dimen.spacer_l),
                    parameter.rightMargin,
                    parameter.bottomMargin
            );
            entryTitle.setLayoutParams(parameter);
        }

        // show media section
        if (mediaList.size() > 0 ) {
            mediaTitle.setVisibility(View.VISIBLE);

            RecyclerView recyclerView = findViewById(R.id.single_entry_media_list);
            recyclerView.setLayoutManager(new GridLayoutManager(this, 3));

            MediaListAdapter mediaListAdapater = new MediaListAdapter(this, mediaList);
            recyclerView.setAdapter(mediaListAdapater);

            final int spacing = getResources().getDimensionPixelSize(R.dimen.spacer_xs);

            // apply spacing to grid cells
            recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
                @Override
                public void getItemOffsets(@NotNull Rect outRect, @NotNull View view, @NotNull RecyclerView parent, @NotNull RecyclerView.State state) {
                    outRect.set(spacing, spacing, spacing, spacing);
                }
            });

        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.entry_map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(callback);
        }
    }
}