package com.team10.placery.ui.entries;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.team10.placery.R;
import com.team10.placery.model.AppDatabase;
import com.team10.placery.model.EntryDAO;
import com.team10.placery.model.EntryWithPaths;

import java.util.Collections;
import java.util.List;

import static com.team10.placery.model.EntryWithPaths.byCreatedDateTime;

public class EntriesFragment extends Fragment {
    private static final String TAG = "Fragment Entries";

    @RequiresApi(api = Build.VERSION_CODES.O)
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState
    ) {
        View root = inflater.inflate(R.layout.fragment_entries, container, false);

        RecyclerView recyclerView = root.findViewById(R.id.entries_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(root.getContext()));

        EntryDAO dao =  AppDatabase.getInstance(root.getContext()).getEntryDAO();
        List<EntryWithPaths> entryList = dao.getAllEntries();
        Collections.sort(entryList, byCreatedDateTime.reversed() );
        EntriesListAdapter entriesListAdapater = new EntriesListAdapter(getContext(), entryList);
        recyclerView.setAdapter(entriesListAdapater);

        return root;
    }
}