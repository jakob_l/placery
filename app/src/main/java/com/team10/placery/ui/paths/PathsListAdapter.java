package com.team10.placery.ui.paths;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.squareup.picasso.Picasso;
import com.team10.placery.R;
import com.team10.placery.SinglePathActivity;
import com.team10.placery.model.EntryWithPaths;
import com.team10.placery.model.PathWithEntries;

import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;

import static com.team10.placery.model.EntryWithPaths.byCreatedDateTime;

public class PathsListAdapter extends RecyclerView.Adapter<PathsListAdapter.MyViewHolder> {

    private static final String TAG = "ListAdapter";

    private List<PathWithEntries> pathsData;

    private Context context;


    public PathsListAdapter(Context context,List<PathWithEntries> myDataset) {

        this.pathsData = myDataset;
        this.context = context;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView date;
        public ImageView thumb;
        public TextView counter;
        public CardView pathCard;

        public MyViewHolder(MaterialCardView v) {
            super(v);
            title = v.findViewById(R.id.path_title);
            date = v.findViewById(R.id.path_date);
            date = v.findViewById(R.id.path_date);
            counter = v.findViewById(R.id.entry_counter);
            thumb = v.findViewById(R.id.path_image);
            pathCard = itemView.findViewById(R.id.card);
        }
    }

    @Override
    public PathsListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MaterialCardView view = (MaterialCardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.path_list_item, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(view);
        return viewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        List<EntryWithPaths> entryList = pathsData.get(position).entriesWithPaths;
        Collections.sort(entryList, byCreatedDateTime );
        DateTimeFormatter dtfy = DateTimeFormatter.ofPattern("dd MMM, uuuu");
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd MMM");
        LocalDateTime pathStartDate = entryList.get(0).getCreatedLocalDateTime();
        String pathDate = null;
        LocalDateTime pathEndDate = entryList.get(entryList.size()-1).getCreatedLocalDateTime();
        if (Period.between(pathStartDate.toLocalDate(), pathEndDate.toLocalDate()).getDays() > 1) {
            pathDate = pathStartDate.format(dtf) + " - " + pathEndDate.format(dtfy);
        } else {
            pathDate = pathStartDate.format(dtfy);
        }
        holder.title.setText(pathsData.get(position).path.title);
        holder.date.setText(pathDate);
        holder.counter.setText(String.valueOf(entryList.size()));
        if (pathsData.get(position).path.mainImage != null) {
            Picasso
                    .get()
                    .load(pathsData.get(position).path.mainImage)
                    .fit()
                    .centerCrop()
                    .into(holder.thumb);
            holder.thumb.setVisibility(View.VISIBLE);
        }

        holder.pathCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id = pathsData.get(position).path.ID.toString();
                Log.d(TAG, "onClick: clicked on: Path with ID " + id);
                Intent intent = new Intent(context, SinglePathActivity.class);
                intent.putExtra("pathId" , id);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {

        return pathsData.size();
    }
}