package com.team10.placery.ui.entry;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.team10.placery.R;
import com.team10.placery.model.Media;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class MediaListAdapter extends RecyclerView.Adapter<MediaListAdapter.MyViewHolder> {
    private static final String TAG = "MediaListAdapter";

    private final List<Media> mediaData;
    private final Context context;

    public MediaListAdapter(Context context, List<Media> media) {
        this.mediaData = media;
        this.context = context;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView preview;
        public ImageView iconView;
        public ConstraintLayout mediaItem;

        public MyViewHolder(ConstraintLayout v) {
            super(v);
            mediaItem = v;
            preview = v.findViewById(R.id.media_item_preview);
            iconView = v.findViewById(R.id.media_item_icon);
        }
    }

    @NotNull
    @Override
    public MediaListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ConstraintLayout view = (ConstraintLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.media_list_item, parent, false);
        return new MyViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NotNull MyViewHolder holder, int position) {
        String mediaType = mediaData.get(position).type;
        int icon = R.drawable.ic_entry_of_path;
        switch (mediaType) {
            case "image":
                icon = R.drawable.baseline_insert_photo_black_24dp;
                holder.iconView.setColorFilter(ContextCompat.getColor(context, R.color.white));
                break;
            case "song": icon = R.drawable.baseline_music_note_black_24dp; break;
            default:
                throw new IllegalStateException("Unexpected value: " + mediaType);
        }

        holder.iconView.setImageResource(icon);

        if (mediaData.get(position).type.equals("image")) {
            Log.d(TAG, "url: " + mediaData.get(position).url);
            Picasso
                    .get()
                    .load(mediaData.get(position).url)
                    .fit()
                    .centerCrop()
                    .into(holder.preview);
        }

//        holder.entryCard.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String id = mediaData.get(position).ID.toString();
//                Log.d(TAG, "onClick: clicked on: Media with ID " + id);
//                Intent intent = new Intent(context, SingleEntryActivity.class);
//                intent.putExtra("mediaId", id);
//                context.startActivity(intent);
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return mediaData.size();
    }
}