package com.team10.placery.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import java.time.LocalDateTime;

@Entity(primaryKeys = {"pathID", "entryID"},
        tableName = "pathsEntries",
        indices = {
            @Index(value="pathID"),
            @Index(value="entryID")
        }
)
public class PathsEntries {

    @NonNull
    public Integer pathID;
    @NonNull
    public Integer entryID;
}