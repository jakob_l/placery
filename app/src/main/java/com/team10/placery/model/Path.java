package com.team10.placery.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import java.time.LocalDateTime;

@Entity(tableName = "paths")
public class Path {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    public Integer ID;
    public String title;
    public String subtitle;
    public String mainImage;
}