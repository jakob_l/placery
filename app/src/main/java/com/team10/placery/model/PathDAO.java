package com.team10.placery.model;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

@Dao
public interface PathDAO {
    @Insert
    public void insert(Path path);

    @Update
    public void update(Path path);

    @Delete
    public void delete(Path path);

    @Transaction
    @Query("SELECT * FROM paths")
    public List<PathWithEntries> getAllPathEntries();

    @Transaction
    @Query("SELECT * FROM paths WHERE ID = :id")
    public PathWithEntries getPathByID(String id);



}