package com.team10.placery.model;

import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;

@RequiresApi(api = Build.VERSION_CODES.O)
public class EntryWithPaths {
    @Embedded public Entry entry;
    @Relation(
            parentColumn = "ID",
            entityColumn = "entryID"
    )
    public List<Media> media;
    @Relation(
            parentColumn = "ID",
            entityColumn = "ID",
            entity = Path.class,
            associateBy = @Junction(
                    value = PathsEntries.class,
                    parentColumn = "entryID",
                    entityColumn = "pathID"
            )
    )
    public List<Path> paths;

    public LocalDateTime getCreatedLocalDateTime() {
        return entry.createDate;
    }

    public static Comparator<EntryWithPaths> byCreatedDateTime = (entry1, entry2) -> {
        if (entry1.getCreatedLocalDateTime().isBefore(entry2.getCreatedLocalDateTime())) return -1;
        else return 1;
    };
}
