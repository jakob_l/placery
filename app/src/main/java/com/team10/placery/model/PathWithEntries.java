package com.team10.placery.model;

import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;

@RequiresApi(api = Build.VERSION_CODES.O)
public class PathWithEntries {
    @Embedded public Path path;
    @Relation(
            parentColumn = "ID",
            entityColumn = "ID",
            entity = Entry.class,
            associateBy = @Junction(
                    value = PathsEntries.class,
                    parentColumn = "pathID",
                    entityColumn = "entryID"
            )
    )
    public List<EntryWithPaths> entriesWithPaths;
}
