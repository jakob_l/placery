package com.team10.placery.model;
import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Entry.class, Path.class, Media.class, PathsEntries.class}, version = 5)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase instance;
    private static String db_name = "placery.sqlite";

    protected AppDatabase() {}

    public abstract EntryDAO getEntryDAO();
    public abstract PathDAO getPathDAO();

    public static AppDatabase getInstance(Context context) {
        if(instance == null) {
            instance = Room.databaseBuilder(context, AppDatabase.class, db_name)
                .allowMainThreadQueries()
                .createFromAsset(db_name)
                .fallbackToDestructiveMigration() // needed to prevent app from crashing despite missing migration
                .build();
        }
        return instance;
    }
}