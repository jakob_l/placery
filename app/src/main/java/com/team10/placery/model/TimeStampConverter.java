package com.team10.placery.model;

import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.room.TypeConverter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RequiresApi(api = Build.VERSION_CODES.O)
public class TimeStampConverter {

    DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;

    @TypeConverter
    public LocalDateTime toDateTime (String dateString){
        LocalDateTime dateTime = formatter.parse(dateString, LocalDateTime::from);
        return dateTime;
    }

    @TypeConverter
    public String toString (LocalDateTime dateTime){
        return dateTime.format(formatter);
    }
}