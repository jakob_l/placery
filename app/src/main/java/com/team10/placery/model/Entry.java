package com.team10.placery.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.time.LocalDateTime;

@Entity(tableName = "entries")
public class Entry {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    public Integer ID;
    public String title;
    public String subtitle;
    @TypeConverters({TimeStampConverter.class})
    public LocalDateTime createDate;
    public Float lon;
    public Float lat;
    public String locationName;
    public String textContent;
    public String mainImage;
}