package com.team10.placery.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.time.LocalDateTime;

@Entity(tableName = "media")
public class Media {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    public Integer ID;
    public String type;
    public String title;
    public String url;
    @TypeConverters({TimeStampConverter.class})
    public LocalDateTime addDate;
    public Integer entryID;

    public LocalDateTime getAddedLocalDateTime() {
        return addDate;
    }
}