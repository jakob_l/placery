package com.team10.placery.model;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;
import java.util.List;

@Dao
public interface EntryDAO {
    @Insert
    public void insert(Entry entry);

    @Update
    public void update(Entry entry);

    @Delete
    public void delete(Entry entry);

    @Transaction
    @Query("SELECT * FROM entries")
    public List<EntryWithPaths> getAllEntries();

    @Transaction
    @Query("SELECT * FROM entries WHERE ID = :id")
    public EntryWithPaths getEntryByID(String id);
}