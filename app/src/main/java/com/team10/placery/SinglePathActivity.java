package com.team10.placery;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RoundCap;
import com.squareup.picasso.Picasso;
import com.team10.placery.model.AppDatabase;
import com.team10.placery.model.EntryWithPaths;
import com.team10.placery.model.PathDAO;
import com.team10.placery.model.PathWithEntries;
import com.team10.placery.ui.entries.EntriesListAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.google.android.gms.maps.model.JointType.ROUND;
import static com.team10.placery.model.EntryWithPaths.byCreatedDateTime;
import static java.security.AccessController.getContext;

@RequiresApi(api = Build.VERSION_CODES.O)
public class SinglePathActivity extends AppCompatActivity {

    private static final String TAG = "SinglePathActivity";

    private Float lat = null;
    private Float lon = null;


    private List<EntryWithPaths> entryList;

    private final OnMapReadyCallback callback = gMap -> {
        try {
            // Customise the styling of the base map
            boolean success = gMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getApplicationContext(), R.raw.style_json));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ");
        }

        gMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        gMap.getUiSettings().setMyLocationButtonEnabled(true);

        List<LatLng> pathPointList = new ArrayList<>();
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        // Loop over path entries to draw MapMarker on Map
        for(EntryWithPaths entryEl: entryList){
            LatLng latLngEntry = new LatLng(entryEl.entry.lat, entryEl.entry.lon);
            pathPointList.add(latLngEntry);
            builder.include(latLngEntry);
            int icon = R.drawable.ic_entry_mapmarker;
            gMap.addMarker(
                    new MarkerOptions().icon(bitmapDescriptorFromVector(this, icon))
                            .position(latLngEntry)
                            .title(entryEl.entry.title));
        }

        LatLngBounds bounds = builder.build();
        gMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 250));

        Polyline pathPolyline = gMap.addPolyline(new PolylineOptions()
                .clickable(true)
                .color(R.color.primaryColor)
                .width(4)
                .endCap(new RoundCap())
                .endCap(new RoundCap())
                .jointType(ROUND)
                .geodesic(true)
                .addAll(pathPointList)
        );
    };

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId ){
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0,0,vectorDrawable.getIntrinsicWidth(),vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),vectorDrawable.getIntrinsicHeight(),Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_path_single_view);

        Intent intent = getIntent();
        String id = intent.getStringExtra("pathId");
        Log.d(TAG, "ID: " + id);

        PathDAO dao =  AppDatabase.getInstance(getApplicationContext()).getPathDAO();
        PathWithEntries pathWithEntries = dao.getPathByID(id);

        TextView pathTitle = findViewById(R.id.single_path_title);
        TextView pathSubtitle = findViewById(R.id.single_path_subtitle);

        pathTitle.setText(pathWithEntries.path.title);
        pathSubtitle.setText(pathWithEntries.path.subtitle);

        entryList = pathWithEntries.entriesWithPaths;
        lat = entryList.get(0).entry.lat;
        lon = entryList.get(0).entry.lon;

        RecyclerView recyclerView = this.findViewById(R.id.path_entries_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        Collections.sort(entryList, byCreatedDateTime.reversed() );
        EntriesListAdapter entriesListAdapater = new EntriesListAdapter(this, entryList);
        recyclerView.setAdapter(entriesListAdapater);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.entry_map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(callback);
        }
    }
}
