DROP TABLE IF EXISTS entries;
CREATE TABLE entries (
  ID INTEGER NOT NULL PRIMARY KEY,
  title TEXT,
  subtitle TEXT,
  createDate TEXT,
  lon REAL,
  lat REAL,
  locationName TEXT,
  textContent TEXT,
  mainImage TEXT
);

INSERT INTO entries VALUES(1, 'Running along the Elbe', 'best long run ever', '2020-11-01T07:30:00.000', 13.698277, 51.067362, 'Am Elbeufer, Dresden', 'Alice felt that the corners: <em>next walking about the soldiers</em> carrying the best to turn round goes the Dormouse shook the roof. There was it, while she could, for croqueting one of WHAT? thought this cat may kiss my adventures beginning from this was now you fair warning, shouted in my shoulders. <br><br> Oh, my way all sorts of the only look at last, more till the thought it in a low voice, "Your hair has won?" This did not even know you''re to stand on the happy summer day: The King''s crown on. I fancy "Who''s to be a very meekly: I''m getting extremely small again." She pitied him into the time to fly up now, she said the King eagerly, and saying to lie down to herself, after such sudden violence that you think I used--and I only of the Hatter shook itself. Then it trot away into its dinner, and shoes done with a mile high," added the BEST butter, you could not have lessons to say what it gloomily: then they met those long enough." Alice said to try and stupid), whether she was.', 'https://source.unsplash.com/91Xo21oKLcA');
INSERT INTO entries VALUES(2, 'Morning @Augustusbrücke', 'lets get this started.', '2020-11-01T07:00:00.000', 13.739588, 51.055089, 'Augustusbrücke, Dresden', '<h1>Emoji Test</h1>For this the text I wanted to test emojis 🚀👻 because I already know that the basic html <em>tags</em> are <h2>working<h2>', NULL);
INSERT INTO entries VALUES(3, 'Still Running️', 'como díos manda!', '2020-11-01T08:00:00.000', 13.656093, 51.092892, 'Am Elbeufer, Radebeule', 'Das ist auch Text', 'https://source.unsplash.com/PHIgYUGQPvU');
INSERT INTO entries VALUES(4, 'Jammin'' with the crow bros', 'just chillin', '2020-11-05T10:10:00.000', 13.744568, 51.056007, 'Neustadt, Dresden', 'Das ist auch Text aber vieeeeeeeeeeeeeeeeeeeeel länger. Wirklich! mit <strong>tags</strong>',  'https://source.unsplash.com/a1LVsvM_zuE');
INSERT INTO entries VALUES(5, 'Workshop im Hülße Bau', NULL, '2020-11-06T14:30:00.000', 13.723777463744856, 51.029227911639715, 'Plauen, Dresden', NULL, NULL);
INSERT INTO entries VALUES(6, 'The early bird gets the worm', NULL, '2020-12-01T05:30:00.000', 13.766316, 51.036500, 'Am Elbeufer, Dresden', NULL, 'https://source.unsplash.com/wzYC6rOdKTI');
INSERT INTO entries VALUES(7, 'What a Walk in the Park', NULL, '2020-12-06T17:30:00.000', 13.752443, 51.041994, 'Großer Garten, Dresden',  NULL, 'https://source.unsplash.com/SBiVq9eWEtQ');
INSERT INTO entries VALUES(8, 'Kick-off im Georg-Schumann-Bau', NULL, '2020-11-01T14:30:00.000', 13.72233979483764, 51.0292886586002, 'Plauen, Dresden', NULL, NULL);
INSERT INTO entries VALUES(9, 'Singing in the Rain️', NULL, '2020-12-20T15:30:00.000', 13.737601, 51.053421, 'Neustadt, Dresden', NULL, 'https://source.unsplash.com/5hHCPRuIq2w');
INSERT INTO entries VALUES(10, 'On top of the world!', NULL, '2020-09-30T08:30:00.000', 13.61259075932156, 47.47423326964993, 'Dachstein, Ramsau', NULL, 'https://source.unsplash.com/FGKhgu5s1Zk');
INSERT INTO entries VALUES(11, 'Looking for Shade', NULL, '2020-08-02T14:10:00.000', 16.44019859164761, 43.508208604071655, 'Old Town, Split', NULL, 'https://source.unsplash.com/cUMy_phPN8E');
INSERT INTO entries VALUES(12, 'Sea!!', NULL, '2020-08-01T09:10:00.000', 16.38917307303795, 43.50875824871996, 'Park Suma Marjan, Split', NULL, 'https://source.unsplash.com/Vr0AmflgB40');
INSERT INTO entries VALUES(13, 'Small Islands in the big sea', NULL, '2020-08-03T15:32:00.000', 15.215871761936695, 43.86750458079002, 'National Park Kornati, Zadar', NULL, 'https://source.unsplash.com/Zr-GvsFf6NA');
INSERT INTO entries VALUES(14, 'In the woods', NULL, '2020-08-03T15:21:00.000', 14.229645734215854, 50.891407187051556, 'Schmilka, Sächsische Schweiz', NULL, 'https://source.unsplash.com/zy-6-5NyAFk');
INSERT INTO entries VALUES(15, 'The cute ferry', NULL, '2020-08-03T16:32:00.000', 14.228904532021936, 50.89047840682393, 'Schmilka, Sächsische Schweiz', NULL, 'https://source.unsplash.com/0snEiDycKYo');
INSERT INTO entries VALUES(16, 'On the rocks', NULL, '2020-08-03T11:41:00.000', 14.250302230235873, 50.891793376789636, 'Kipphornaussicht, Sächsische Schweiz', NULL, 'https://source.unsplash.com/DTRrwgAxbGY');
INSERT INTO entries VALUES(17, 'Moonrise', NULL, '2020-10-02T19:30:00.000', 13.654314862687988, 47.37704169983763, 'Hochwurzn', NULL, 'https://source.unsplash.com/12mvi9SDl7I');

DROP TABLE IF EXISTS paths;
CREATE TABLE paths (
  ID INTEGER NOT NULL PRIMARY KEY,
  title TEXT DEFAULT NULL,
  subtitle TEXT DEFAULT NULL,
  mainImage TEXT DEFAULT NULL
);

INSERT INTO paths VALUES(1, 'Croatia Trip', 'What a summer!', 'https://source.unsplash.com/ohoBiu7_KNc');
INSERT INTO paths VALUES(2, 'Hike Sächsische Schweiz', 'Exploration Time!', 'https://source.unsplash.com/e13EWX0d1Vw');
INSERT INTO paths VALUES(3, 'Sunday Long Run', 'Running down the river', 'https://source.unsplash.com/mNGaaLeWEp0');
INSERT INTO paths VALUES(4, 'ACFS 2020', 'I <3 the Alps', 'https://source.unsplash.com/QOuFxUEFm18');
INSERT INTO paths VALUES(5, 'TUD Kick-off', 'Let''s get this thing started!', NULL);

DROP TABLE IF EXISTS pathsEntries;
CREATE TABLE pathsEntries (
  pathID INTEGER NOT NULL,
  entryID INTEGER NOT NULL,
  PRIMARY KEY(pathID, entryID)
);

INSERT INTO pathsEntries VALUES(1, 11);
INSERT INTO pathsEntries VALUES(1, 12);
INSERT INTO pathsEntries VALUES(1, 13);
INSERT INTO pathsEntries VALUES(2, 14);
INSERT INTO pathsEntries VALUES(2, 15);
INSERT INTO pathsEntries VALUES(2, 16);
INSERT INTO pathsEntries VALUES(3, 1);
INSERT INTO pathsEntries VALUES(3, 2);
INSERT INTO pathsEntries VALUES(3, 3);
INSERT INTO pathsEntries VALUES(4, 10);
INSERT INTO pathsEntries VALUES(4, 17);
INSERT INTO pathsEntries VALUES(5, 8);
INSERT INTO pathsEntries VALUES(5, 5);

DROP TABLE IF EXISTS media;
CREATE TABLE media (
  ID INTEGER NOT NULL PRIMARY KEY,
  type TEXT DEFAULT NULL,
  title TEXT DEFAULT NULL,
  url TEXT DEFAULT NULL,
  addDate TEXT,
  entryID INTEGER
);

INSERT INTO media VALUES(1, 'image', NULL, 'https://source.unsplash.com/XiZ7pRvCzro', '2020-11-01T07:31:00.000', 1);
INSERT INTO media VALUES(2, 'image', NULL, 'https://source.unsplash.com/wmdcUQ0CJ4c', '2020-11-01T07:31:00.000', 1);
INSERT INTO media VALUES(3, 'image', NULL, 'https://source.unsplash.com/LdhTorI9T-o', '2020-11-01T07:31:00.000', 1);
INSERT INTO media VALUES(4, 'image', NULL, 'https://source.unsplash.com/LdhTorI9T-o', '2020-11-01T07:31:00.000', 1);
INSERT INTO media VALUES(5, 'image', NULL, 'https://source.unsplash.com/mODxn7mOzms', '2020-11-01T07:31:00.000', 9);
INSERT INTO media VALUES(6, 'image', NULL, 'https://source.unsplash.com/yeN9XfiUafY', '2020-11-01T07:31:00.000', 9);
INSERT INTO media VALUES(7, 'song', NULL, 'https://spotify.com', '2020-11-01T07:31:00.000', 1);
INSERT INTO media VALUES(8, 'song', NULL, 'https://spotify.com', '2020-11-01T07:31:00.000', 9);
INSERT INTO media VALUES(9, 'image', NULL, 'https://source.unsplash.com/Nw_D8v79PM4', '2020-11-01T07:31:00.000', 9);
INSERT INTO media VALUES(10, 'image', NULL, 'https://source.unsplash.com/2_gJeve_CBY', '2020-11-01T07:31:00.000', 9);
