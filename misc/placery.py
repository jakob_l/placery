from os import read
import sqlite3
from sqlite3 import Error

def sql_connection():

    try:
        con = sqlite3.connect('app/src/main/assets/placery.sqlite')
        return con
    except Error:
        print(Error)

def sql_table(con):

    cursorObj = con.cursor()

    fd = open('misc/placery.sql', 'r', encoding="utf8")
    sqlFile = fd.read()
    cursorObj.executescript(sqlFile)
    fd.close()

    con.commit()

con = sql_connection()

sql_table(con)