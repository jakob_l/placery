# Placery

a spatial diary app
for the mobile Cartography class

## Todos
- [x] reading data from database
- [x] fix room schema for joined tables
- [x] routing (how go to the map page)
- [x] checking if cloning the project works for you as well (local properties/settings e.g. maps api key)
- [x] git workflow?
- [x] Create Splash Screen
- [x] spreading good mood and smiles :)

## Requirements for submission
- [x] use of different Views and ViewGroups including a ListView or RecyclerView
- [x] use of Intents
- [x] integration of a map with an own map style
- [x] use of GPS
- [x] use of a local SQLite database
- [x] design and integration of an own app icon

## Commands to connect your phone via WIFI with Android Studio

`adb tcpip 5555 && adb connect 192.168.xxx.xxx:5555`
(the phone's ip address will be different depending on the network you're connected to)

## Create SQLITE database
- with python: `python misc/placery.py`
- on mac os x: `cat misc/placery.sql | sqlite3 app/src/main/assets/placery.sqlite`
